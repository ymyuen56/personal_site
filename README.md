# personal_site

## Node Version

- 21.7.3

## Angular Version

- 17.3.5

## 安装依赖

- npm install

## 启动项目

- npm start

## 打包项目

- ng build

## deploy to firebase

- npm install -g firebase-tools
- firebase login
- firebase init
- firebase deploy
