import { Component, ElementRef, ViewChild } from '@angular/core';
import { NgOptimizedImage } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    NgOptimizedImage,
    RouterLink
  ],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {
  @ViewChild("menu")
  menu!: ElementRef<HTMLDivElement>;

  @ViewChild("buger")
  bugger!: ElementRef<HTMLAnchorElement>;


  toggleIsActive(): void {
    this.menu.nativeElement.classList.toggle('is-active')
    this.bugger.nativeElement.classList.toggle('is-active')
  }

}
