import { Component, ElementRef, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-callapsible',
  standalone: true,
  imports: [],
  templateUrl: './callapsible.component.html',
  styleUrl: './callapsible.component.css'
})
export class CallapsibleComponent {
  @Input() title: string = '';
  @Input() details: string[] = [];
  @ViewChild('collapsibleItem') collapsibleItem!: ElementRef<HTMLDivElement>;

  onClick() {
    this.collapsibleItem.nativeElement.classList.toggle('open');
    const content = this.collapsibleItem.nativeElement.nextElementSibling as HTMLElement;
    if (!content.style.maxHeight || content.style.maxHeight === "0px") {
      content.style.maxHeight = `${content.scrollHeight}px`;
    } else {
      content.style.maxHeight = "0px";
    }
  }
}
