import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallapsibleComponent } from './callapsible.component';

describe('CallapsibleComponent', () => {
  let component: CallapsibleComponent;
  let fixture: ComponentFixture<CallapsibleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CallapsibleComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CallapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
