import { Component } from '@angular/core';
import { CallapsibleComponent } from '../callapsible/callapsible.component';

@Component({
  selector: 'app-work',
  standalone: true,
  imports: [CallapsibleComponent],
  templateUrl: './work.component.html',
  styleUrl: './work.component.css'
})
export class WorkComponent {
  workExperiences: { title: string, details: string[] }[] = [
    {
      title: "Meta Air Labs Limited, Analyst Programmer — June 2022 - December 2023",
      details: [
        "Implemented blockchain (Cardano and Ripple) transaction signature service",
        "Implemented blockchain (Cardano and Ripple) transaction broadcast service",
        "Integrated third-party API to support blockchain (Cardano and Ripple) transaction monitoring service"
      ]
    },
    {
      title: 'IDEOGRAMS  (Former Alliance Computer System Limited), Analyst Programmer 1 — January 2022 - Present',
      details: [
        "Designed and Implemented the control function of Nimble Streamer (Live Streaming Platform)",
        "Implemented DRM (Digital Rights Management) Proxy Server",
        "Designed and Implemented a ticket system for live streaming",
        "Integrated AWS media service and DRM service to support live streaming service",
        "Performed control function design of AWS media service"]

    },
    {
      title: 'Alliance Computer System Limited , Analyst Programmer 1 — July 2019 - December 2021',
      details:
        [
          "Implemented function enhancement to PBX system and PBX administration system",
          "Designed and Implemented Caller Information supporting system ( PBX supporting system)",
          "Provided debugging to PBX and any PBX supporting System",
          "Participated in ad hoc support",
          "Provided System deployment and overall testing",
          "Performed user requirement collection and function prototype design"
        ]
    },
    {
      title: "SHIJI Information Technology (HK) , Programmer - June 2018 - May 2019",
      details: [
        "Implemented function enhancement to POS system",
        "Provided debugging to existing POS system",
        "Prepared user guide document for the new function of POS system",
        "Participate in the user Support"
      ]
    },
    {
      title: "Underwriters Laboratories (HK) , IT Intern — July 2017 - August 2017",
      details: [
        "Provided technical support to the company",
        "Preformed IT equipment and computer inventory stock taking",
        "Provided ad hoc administration support of the team"
      ]
    },
    {
      title: "Keysoc Limited , Mobile Application Development Intern — June 2015 - July 2015",
      details: [
        "Implemented the User Interface of IOS application",
        "Designed an iOS application prototype for Object identification to distinguish the official license",
        "Programed IoT system with Raspberry PI to control access of a mail box"
      ]
    }

  ]
}
