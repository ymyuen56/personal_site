import { Component } from '@angular/core';
import { EducationCardComponent } from '../education-card/education-card.component';

@Component({
  selector: 'app-education',
  standalone: true,
  imports: [
    EducationCardComponent
  ],
  templateUrl: './education.component.html',
  styleUrl: './education.component.css'
})
export class EducationComponent {
  educationList: { imagePath: string, school: string, course: string, department: string, result: string }[] = [
    {
      imagePath: "/assets/polyu.png",
      school: "The Hong Kong Polytechnic University",
      course: "B.Sc.( Hons ) In Information Technology with Second Class Honors , Division 2",
      department: "Department Of Computing",
      result: "GPA: 2.92"
    },
    {
      imagePath: "/assets/ive.png",
      school: "Hong Kong Institute of Vocation Education ( Shan Tin)",
      course: "Higher Diploma In Computer And Information Engineering",
      department: "Department Of Engineering",
      result: "Grading: Distinction"
    },
    {
      imagePath: "/assets/spring_boot.png",
      school: "[NEW] Spring Boot 3, Spring 6 & Hibernate for Beginners",
      course: "lecturer: Chad Darby",
      department: "Udemy",
      result: "Completed at March 28, 2024"
    }
  ]

}
