import { Component, Input } from '@angular/core';
import { NgOptimizedImage } from '@angular/common';

@Component({
  selector: 'app-education-card',
  standalone: true,
  imports: [
    NgOptimizedImage
  ],
  templateUrl: './education-card.component.html',
  styleUrl: './education-card.component.css'
})
export class EducationCardComponent {

  @Input() course: string = "";
  @Input() school: string = "";
  @Input() department: string = "";
  @Input() imagePath: string = "";
  @Input() result: string = "";

}
