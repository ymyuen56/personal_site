import { Routes } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { ContactComponent } from './component/contact/contact.component';
import { WorkComponent } from './component/work/work.component';
import { EducationComponent } from './component/education/education.component';

export const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: "work",
        component: WorkComponent
    },
    {
        path: "education",
        component: EducationComponent
    }
];
